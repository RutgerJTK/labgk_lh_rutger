#!/bin/bash

## this is a testing file to analyse the variant calls with bcftools using the seqnext files. 

vcfs="/mnt/workspace/Rutger/lifelines/vcf_seqnext/45-vcf/"
call_array=()
echo here!
for vcf in "$vcfs"*
do
	sample=(`echo "$vcf" | cut -s  -d '-' -f 3 | cut -s -d '.' -f 1`)
	echo $sample
	
	bgzip -c "$vcf" > "$vcf".gz
	tabix -p vcf "$vcf".gz 
	call_array+=("$vcf".gz)

	cp "$vcf"* /mnt/workspace/Rutger/varcals/"$sample"_calls
	cd /mnt/workspace/Rutger/varcals/"$sample"_calls
    bgzip -c "$sample"_lf.vcf > "$sample"_lf.vcf.gz        
	bgzip -c "$sample"_fb.vcf > "$sample"_fb.vcf.gz        
	bgzip -c "$sample"_vd.vcf > "$sample"_vd.vcf.gz
    tabix -p vcf "$sample"_lf.vcf.gz     
	tabix -p vcf "$sample"_vd.vcf.gz
	tabix -p vcf "$sample"_fb.vcf.gz
	
	bcftools isec -c all -p all__"$sample" -n=3 "$sample"_lf.vcf.gz "$sample"_vd.vcf.gz  "$sample"_fb.vcf.gz 
	bcftools isec -c all -p SeqNext_calls_"$sample" -n=4   "$sample"_lf.vcf.gz "$sample"_vd.vcf.gz  "$sample"_fb.vcf.gz
	bcftools merge  "$sample"_lf.vcf.gz "$sample"_vd.vcf.gz  "$sample"_fb.vcf.gz > all_calls_"$sample".vcf
	echo  "$sample": 	
	echo -------------------------------------------------

done
echo ---------------------------------
echo "${call_array[*]}"
echo "${call_array[1]}"
cd /mnt/workspace/Rutger/lifelines/vcf_seqnext/45-vcf/
rm *.gz
rm *.tbi


