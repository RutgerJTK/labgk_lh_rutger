# LABGK_LH_Rutger

# Git project to back-up the NGS data analysis workflow scripts.

# This project has 3 files only:
# cat_reads.py is a python script which runs on python 3.8 to concatenate read data.
# bcf_man.sh is a script only used to test bcftools vcf file manipulation with using seqnext data.
# forcom.sh is the entire pipeline, incorporating cat_reads.py. 

# disclaimer: to run this pipeline read data and several other tools are required to be pre-installed. 
