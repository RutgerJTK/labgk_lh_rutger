#!/bin/bash

################################################################################
# Author: Rutger Kemperman
# Aim: A pipeline to automatically analyse and call variants on DNA read data. 
# Date: 20-10-2020 - 5-2-2021
#
################################################################################

## define paths
reads_dir="/mnt/workspace/Rutger/cat_reads/"		# path with all concatenated reads
#reads_dir="/mnt/workspace/Rutger/somereads/"		# path with a few reads for testing
out_dir="/mnt/workspace/Rutger/varcals/"			# base path to where output is directed
ref_fasta="/home/rutger.kemperman@researchenvironment.org/refFasta/Homo_sapiens.GRCh37.dna.primary_assembly.fa"		# path to assembly reference fasta
bed_file="/mnt/workspace/Rutger/bedFiles/GRCh37.bed"			# path to bed file --> targets for DNA
vcfs="/mnt/workspace/Rutger/lifelines/vcf_seqnext/45-vcf/"		# used with bcftools isec
snpeff_dir="/mnt/workspace/Rutger/SnpEff/"			# required to run SnpEff

## define variables
reads1=()
reads2=()
read_abbrevs=()
call_array=()
samples_array=()
x=0			# A counter used to loop over vcf files

## call cat_reads.py 
python3 cat_reads.py

## get read names and store in arrays
for entry in "$reads_dir"*			# loop over all reads, getting abbreviations and sample names and stores them in an array
do 
	n=(`echo "$entry" | cut -s -d "/" -f 6 | cut -s -d "." -f 1`)	# gets read 1 and read 2
	if [[ "$n" == *_R1 ]]; then
		reads1+=("$n")
		sample_abbrev=(`echo "$n" | cut -s -d "-" -f 2`)			# gets read abbreviation and stores in array
		read_abbrevs+=("$sample_abbrev")
	else
		reads2+=("$n")
	fi
done
echo "${reads1[*]}" 

## go to images
cd ~/images
ls
### for loop: for item in array. don't forget to do it numerical as well. works easier. 
for i in $(seq 1 "${#reads1[@]}")		# loops over each element in read array, this way looping over every sample
do
	sample="${read_abbrevs["x"]}"		# get each sample name
	echo "$sample"
	mkdir /mnt/workspace/Rutger/varcals/"$sample"_info		# make dump folder for additional info produced during the analysis.
	mkdir /mnt/workspace/Rutger/varcals/"$sample"_calls		# make call folder in which all sample specific calls are placed. 
	echo "Clustering:"	# performs Calib clustering module, grouping reads with similar UMI tags together. 
	singularity exec --bind /mnt/workspace:/mnt/workspace/ calib_latest.sif calib -c 2 -f "$reads_dir""${reads1["$x"]}".fastq -r "$reads_dir""${reads2["$x"]}".fastq -l 10 -o ~/images/"${read_abbrevs["x"]}"_R.
	echo "Error-correction:"	# performs Calib error-correction module. Discards reads that don't reach the defaut coverage threshold. 
	singularity exec --bind /mnt/workspace:/mnt/workspace/ calib_latest.sif calib_cons -c ~/images/"$sample"_R.cluster -q "$reads_dir""${reads1["$x"]}".fastq "$reads_dir""${reads2["$x"]}".fastq -o ~/images/"$sample"_R1 ~/images/"$sample"_R2 

    mv "$sample"* ~/tmp			# move all samples to a temporary folder.
    cd ~/tmp

	### BWA and mapping now
	echo "BWA:" 				# mapping of reads with BWA to GRCh37 reference assembly
	bwa mem -t 2 ~/refFasta/Homo_sapiens.GRCh37.dna.primary_assembly.fa ~/tmp/"$sample"_R1.fastq ~/tmp/"$sample"_R2.fastq > ~/tmp/"$sample".sam
	## converting aligned reads from .sam to .bam format, sorting reads and producing an index file.
	echo "SAMtools:"			
	samtools view -S -b "$sample".sam > "$sample".bam
	samtools sort "$sample".bam > "$sample".sorted.bam		
	file="$sample".sorted.bam
	samtools index "$file"	


	echo "Mapping done, on to variant calling" 
	cd /mnt/workspace/Rutger
	## performing variant calling with 4 variant calling tools through the appreci8 singularity image. /mnt/workspace was bound so all files could be reached by the variant calling tools. Files were output to a temporary directory. 
	echo "Lofreq calling:"
	singularity exec --bind /mnt/workspace:/mnt/workspace/ appreci8.sif /dependencies/lofreq/lofreq call --call-indels -f ~/refFasta/Homo_sapiens.GRCh37.dna.primary_assembly.fa ~/tmp/"$file" > ~/tmp/"$sample"_lf.vcf
	echo "VarDict calling:"
	## Note: vardict calling is a bit more complex as it required singularity to bind directories several times in order to reach certain files which vardicts executes. 
	singularity exec --bind /mnt/workspace:/mnt/workspace/ appreci8.sif /dependencies/VarDict/vardict.pl -G ~/refFasta/Homo_sapiens.GRCh37.dna.primary_assembly.fa -f 0.01 -N "$sample"_vd -b ~/tmp/"$file" -c 1 -S 2 -E 3 -g 4 /mnt/workspace/Rutger/bedFiles/GRCh37.bed | singularity exec --bind /mnt/workspace:/mnt/workspace/ appreci8.sif /dependencies/VarDict/teststrandbias.R | singularity exec --bind /mnt/workspace:/mnt/workspace/ appreci8.sif /dependencies/VarDict/var2vcf_valid.pl  -f 0.01 > ~/tmp/"$sample"_vd.vcf 
	echo "Freebayes calling:"	
	singularity exec --bind /mnt/workspace:/mnt/workspace/ appreci8.sif /dependencies/freebayes/bin/freebayes -f ~/refFasta/Homo_sapiens.GRCh37.dna.primary_assembly.fa -F 0.01 ~/tmp/"$file" > ~/tmp/"$sample"_fb.vcf
	echo "SNVer calling:"
	singularity exec --bind /mnt/workspace:/mnt/workspace/ appreci8.sif java -jar /dependencies/SNVer/SNVerIndividual.jar  -i ~/tmp/"$file" -r ~/refFasta/Homo_sapiens.GRCh37.dna.primary_assembly.fa -t 0.01 > ~/tmp/"$sample"_SNVer.vcf
	echo "Done calling. Moving files."
	
	## moving all variant calls and other files either to their respective calls or information directory
	cd ~/tmp
	mv *.vcf /mnt/workspace/Rutger/varcals/"$sample"_calls
	mv "$sample"* /mnt/workspace/Rutger/varcals/"$sample"_info
	echo done with sample "$sample"
	cd ~/images
	let "x++"
done
cd ~/tmp
 
echo -----------------------------------------------
echo done calling, read manipulation and annotation.
echo -----------------------------------------------

## this loop loops over all vcf files in accordance with the sample files, then uses these for bcftools manipulation and SnpEff annotation and filtering. 
for vcf in "$vcfs"*		
do
    sample=(`echo "$vcf" | cut -s  -d '-' -f 3 | cut -s -d '.' -f 1`)	# gets the correct name of each vcf file
    echo $sample

    bgzip -c "$vcf" > "$vcf".gz
    tabix -p vcf "$vcf".gz
    call_array+=("$vcf".gz)
	samples_array+=("$sample")
    cp "$vcf"* /mnt/workspace/Rutger/varcals/"$sample"_calls
    cd /mnt/workspace/Rutger/varcals/"$sample"_calls
	mkdir "$sample"_annotations		# create a directory in each sample calls directory to store the annotations in

	## bcftools manipulation of Lofreq, Freebayes and VarDict calls. Conflicted with SNVer.
    bgzip -c "$sample"_lf.vcf > "$sample"_lf.vcf.gz
    bgzip -c "$sample"_fb.vcf > "$sample"_fb.vcf.gz
    bgzip -c "$sample"_vd.vcf > "$sample"_vd.vcf.gz
    tabix -p vcf "$sample"_lf.vcf.gz
    tabix -p vcf "$sample"_vd.vcf.gz
    tabix -p vcf "$sample"_fb.vcf.gz
    bcftools isec -c all -p all__"$sample" -n=3 "$sample"_lf.vcf.gz "$sample"_vd.vcf.gz  "$sample"_fb.vcf.gz
    bcftools isec -c all -p SeqNext_calls_"$sample" -n=4   "$sample"_lf.vcf.gz "$sample"_vd.vcf.gz  "$sample"_fb.vcf.gz
    bcftools merge  "$sample"_lf.vcf.gz "$sample"_vd.vcf.gz  "$sample"_fb.vcf.gz > all_calls_"$sample".vcf 		# Merged files were ultimately not used anymore as they conflicted with SnpEff.
	cd /mnt/workspace/Rutger/SnpEff

	## Base variant call annotation. "annot" was added to output file name to clarify the annotation step. 
	singularity exec --bind	/mnt/workspace:/mnt/workspace/ SnpEff.sif java -jar -Xmx8g /SnpEff/snpEff.jar -v  GRCh37.75  /mnt/workspace/Rutger/varcals/"$sample"_calls/"$sample"_vd.vcf > ~/tmp/"$sample"_vd_annot.vcf
	singularity exec --bind /mnt/workspace:/mnt/workspace/ SnpEff.sif java -jar -Xmx8g /SnpEff/snpEff.jar -v  GRCh37.75  /mnt/workspace/Rutger/varcals/"$sample"_calls/"$sample"_lf.vcf > ~/tmp/"$sample"_lf_annot.vcf
	singularity exec --bind /mnt/workspace:/mnt/workspace/ SnpEff.sif java -jar -Xmx8g /SnpEff/snpEff.jar -v  GRCh37.75  /mnt/workspace/Rutger/varcals/"$sample"_calls/"$sample"_fb.vcf > ~/tmp/"$sample"_fb_annot.vcf
	
	echo done annotating
	
	## Base annotation filtering. "fil" added to output file name to clarify filtering was performed.
	singularity exec --bind /mnt/workspace:/mnt/workspace/ SnpEff.sif cat ~/tmp/"$sample"_vd_annot.vcf | singularity exec --bind /mnt/workspace:/mnt/workspace/ SnpEff.sif java -jar /SnpEff/SnpSift.jar filter " (( QUAL >= 30 ) & ( AF > 0.02 )) " > ~/tmp/fil_"$sample"_vd_annot.vcf 
    singularity exec --bind /mnt/workspace:/mnt/workspace/ SnpEff.sif cat ~/tmp/"$sample"_lf_annot.vcf | singularity exec --bind /mnt/workspace:/mnt/workspace/ SnpEff.sif java -jar /SnpEff/SnpSift.jar filter " (( QUAL >= 30 ) & ( AF > 0.02 )) " > ~/tmp/fil_"$sample"_lf_annot.vcf
    singularity exec --bind /mnt/workspace:/mnt/workspace/ SnpEff.sif cat ~/tmp/"$sample"_fb_annot.vcf | singularity exec --bind /mnt/workspace:/mnt/workspace/ SnpEff.sif java -jar /SnpEff/SnpSift.jar filter " (( QUAL >= 30 ) & ( AF > 0.02 )) " > ~/tmp/fil_"$sample"_fb_annot.vcf

	echo done filtering
	
	## Extraction of key information. "ex" added to output file name to clarify extraction of info was performed.
	singularity exec --bind /mnt/workspace:/mnt/workspace/ SnpEff.sif java -jar -Xmx8g /SnpEff/SnpSift.jar extractFields  ~/tmp/fil_"$sample"_vd_annot.vcf  "ANN[*].GENE" CHROM AF "ANN[*].EFFECT" "ANN[*].HGVS_C" "ANN[*].HGVS_P" > ~/tmp/fil_"$sample"_vd_ex.txt
    singularity exec --bind /mnt/workspace:/mnt/workspace/ SnpEff.sif java -jar -Xmx8g /SnpEff/SnpSift.jar extractFields  ~/tmp/fil_"$sample"_lf_annot.vcf  "ANN[*].GENE" CHROM AF "ANN[*].EFFECT" "ANN[*].HGVS_C" "ANN[*].HGVS_P" > ~/tmp/fil_"$sample"_lf_ex.txt
    singularity exec --bind /mnt/workspace:/mnt/workspace/ SnpEff.sif java -jar -Xmx8g /SnpEff/SnpSift.jar extractFields  ~/tmp/fil_"$sample"_fb_annot.vcf  "ANN[*].GENE" CHROM AF "ANN[*].EFFECT" "ANN[*].HGVS_C" "ANN[*].HGVS_P" > ~/tmp/fil_"$sample"_fb_ex.txt

    cd ~/tmp
    ## move all annotations to proper directory. 
    mv * /mnt/workspace/Rutger/varcals/"$sample"_calls/"$sample"_annotations/	
done

## cleaning SeqNext .vcf directory so it can be reused in a future.
cd "/mnt/workspace/Rutger/lifelines/vcf_seqnext/45-vcf/" 	
rm *.gz
rm *.tbi

echo -----------------------------------------------
echo all finished, good day!
echo -----------------------------------------------
