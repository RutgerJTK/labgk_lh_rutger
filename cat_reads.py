# @Author: Rutger Kemperman
# @Date: 5-2-2021
# This Python script aims at automatically concatenating DNA read data so its ready for further processing.
# note: call this file by executing "python3 call_vars" . Python version 3 or higher must be used.

import os
import re

"""
Input: raw reads from NGS
Process: puts reads in different lists for processing
Output: sorted lists which correspond to each other
"""	
def get_reads(reads_dir):
	L1R1_list = []
	L1R2_list = []
	L2R1_list = []
	L2R2_list = []
	for read in os.listdir(reads_dir):
		if "L001_R1" in read:
			L1R1_list.append(read)
		elif "L001_R2" in read:
			L1R2_list.append(read)
		elif "L002_R1" in read:
			L2R1_list.append(read)
		elif "L002_R2" in read:
			L2R2_list.append(read)	
	
	# reads were appended arbitratily, sort reads so they line up correctly with each list.
	L1R1_list.sort(); L1R2_list.sort(); L2R1_list.sort(); L2R2_list.sort()

	return L1R1_list, L1R2_list, L2R1_list, L2R2_list


"""
Input: sorted lists with raw reads
Process: concatenates reads from 4 files to 2 files
Output: Concatenated reads are written to cat_dir
Note; the assumption is made that each sample has 4 reads, and the reads are thus lined up correctly.
      if the reads don't line up the whole program will break. 
"""
def cat_reads(reads_dir, cat_dir, L1R1, L1R2, L2R1, L2R2):
	cat_name = re.compile(".*S.{2,3}_")		# forms a regular expression to grab the sample name of each readfile with later. 
	a = 0
	fnames = []
	for i in L1R1:
		match = re.match(cat_name, i)	# uses a regular expression to correctly match the proper name of the readfile. 
		fnames.append(match.group(0))	
	
	print("amount of new files: ", len(fnames))

	#Loop over all files in list; concatenate files corresponding to each other > L1R1 + L2R1, L1R1 + L2R2 
	for i in range(len(L1R1)):		
		# concatenation for Read 1 (R1 suffix) 

		print("Reading, writing "+ fnames[a]+ " Read 1")
		f1 = open(reads_dir+"/"+L1R1[a], "r") 	#path + filename
		text1 = f1.read()
		f2 = open(reads_dir+"/"+L2R1[a], "r") 	#path + filename
		text2 = f2.read()
		#open new file and write to file
		with open(cat_dir+"/"+fnames[a]+"R1.fastq", "w") as outfile:
			outfile.write(text1)
                        outfile.write("/n")
			outfile.write(text2)
			outfile.close()
		f1.close()
		f2.close()

		# concatenation for Read 2 (R2 suffix)
		print("Reading, writing "+ fnames[a]+ " Read 2")
		f1 = open(reads_dir+"/"+L1R2[a], "r")   #path + filename
		text1 = f1.read()
		f2 = open(reads_dir+"/"+L2R2[a], "r")   #path + filename
		text2 = f2.read()
                #open new file and write to file
		with open(cat_dir+"/"+fnames[a]+"R2.fastq", "w") as outfile:
			outfile.write(text1)
                        outfile.write("/n")
			outfile.write(text2)
			outfile.close()
		f1.close()
		f2.close()
		
		print("Done reading and writing "+ fnames[a])
		a += 1

	print("All files are done, stopping")


if __name__ == '__main__':
	print("start")
	reads_dir = r"/home/rutger.kemperman@researchenvironment.org/reads"
	cat_dir = r"/mnt/workspace/Rutger/cat_reads2"
	
	L1R1_list, L1R2_list, L2R1_list, L2R2_list = get_reads(reads_dir)	
	cat_reads(reads_dir, cat_dir, L1R1_list, L1R2_list, L2R1_list, L2R2_list)



